import asyncio
import time
from typing import List

from aiohttp.client import ClientSession, ClientTimeout, ClientResponse

loop = asyncio.get_event_loop()
session_timeout = ClientTimeout(total=10)

urls = [
    'https://google.com',
    'https://facebook.com',
    'https://instagram.com'
] * 1000


def with_client_session(coro):
    async def wrapper(url: str):
        session = ClientSession(timeout=session_timeout)
        result = None

        result = await coro(method=session.get, url=url)

        await session.close()
        return result

    return wrapper


async def generator(subjects: list):
    i = 0
    len_a = len(subjects)
    while i < len_a:
        yield subjects[i]
        i += 1


def done_callback(result: asyncio.Task):
    print(f"[{time.ctime()}] result:", result.result())


async def fetch(url: str):
    await asyncio.sleep(1)
    session = ClientSession()
    response = await session.get(url)
    await session.close()
    return response.status, url.split('.')[0]


def main():
    tasks: List[asyncio.Future] = []

    for url in urls:
        task = asyncio.ensure_future(fetch(url))
        task.add_done_callback(done_callback)
        tasks.append(task)

    return tasks


loop.run_until_complete(asyncio.gather(asyncio.wait(main())))
